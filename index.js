// let data = require("./data");
import  data  from "./data.js";
console.log(data.books[0].author);

// main  Div
let mainDiv = document.createElement("div");
document.body.appendChild(mainDiv);
mainDiv.style.background = "#FFF5E8";
mainDiv.style.display = "flex";
mainDiv.style.flexWrap = "wrap";
mainDiv.style.justifyContent = "space-around"
mainDiv.style.paddingLeft = "10px"

for (let i = 0; i < 8; i++){
    let card = document.createElement("div");
    card.style.width = "24%";
    mainDiv.appendChild(card)
      card.style.height = "300px";
    card.style.background = "white";
    card.style.margin = " 10px 10px 0px 0px ";
    let img = document.createElement("img")
    img.src = data.books[i].image;
    img.style.width = "50%"
    img.style.height="160px"
    card.appendChild(img)
    card.style.display = "flex"
    card.style.flexDirection = "column";
    card.style.alignItems = "center";
    card.style.justifyContent = "center";
    let h4 = document.createElement("h4");
    h4.innerText = data.books[0].title;
    h4.style.width = "100%"
    h4.style.textAlign = "center"
    h4.style.margin="10px"
    card.appendChild(h4)
    let p = document.createElement("p");
    p.innerText = "Auther : " + data.books[i].author;
    card.appendChild(p)
    let btn = document.createElement("button");
    btn.style.background = "#35807C";
    btn.innerText = "Buy Now";
    btn.style.padding = "10px 20px"
    btn.style.borderRadius = "4px"
    btn.style.marginTop="20px"
    btn.style.border = "none"
    btn.style.cursor="pointer"
    card.appendChild(btn)
    card.style.borderRadius = "8px";
}